package ehealth.document.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JOptionPane;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import ehealth.document.ws.Currentlifestatus;
import ehealth.document.ws.Goalsetting;
import ehealth.document.ws.HealthProfileSolution;
import ehealth.document.ws.HealthProfileSolution_Service;
import ehealth.document.ws.Measuredefinition;
import ehealth.document.ws.Userprofile;

/**
 * <p>
 * Lets user to communicate with HealthProfileSolution server to save his
 * information and get the inspirational quote, information about diea and
 * exercise etc. A WebPush polling service is added in this to get the push from
 * server about the user's recent goals.
 * </p>
 * <p>
 * It is commmand line client, that first get the login detail to login in a
 * user. It, then, shows different options to use different functions.
 * </p>
 * It has following functions
 * 1. Login
 * 2. Input measure data
 * 3. Set a goal
 * 4. Request awareness News (quote)
 * 5. Persuasive strategy
 * 6. Feedback about your health
 * 7. Share on Twitter your recent measure 
 * 8. Logout
 * 9. Exit
 * 
 * 
 * @author Jasim
 *
 */
public class LifeCoach {

	public static Integer PushReminders_Interval = 1; // 1 second
	public static HealthProfileSolution_Service service;
	public static HealthProfileSolution servicePort;

	public static String username = "";
	public static String password = "";
	public static String login_Id = "";
	public static Integer fsmControl = 0;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		service = new HealthProfileSolution_Service();
		servicePort = service.getHealthProfileSolutionImplPort();

		int switch_option = 0;
		int exit = 0;

		PushReminders pr_thread = new PushReminders();
		pr_thread.start();

		while (true) {

			if (login_Id == "")
				login();
			else
				printMenuPage();

			BufferedReader buffer = new BufferedReader(new InputStreamReader(
					System.in));
			try {
				switch_option = Integer.parseInt(buffer.readLine());
			} catch (NumberFormatException ex) {
				ex.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			switch (switch_option) {

			case 1: {
				if (login_Id != "")
					break;

				System.out.println("Please enter your username: ");
				// username = buffer.readLine();
				username = "Pogba.Paul";
				System.out.println("Please enter your password: ");
				password = buffer.readLine();

				login_Id = servicePort.login(username, password);

				System.out.println("************\n" + login_Id
						+ "\n************");

			}
				break;

			case 2: {

				List<Measuredefinition> mdefs = servicePort
						.getAllMeasureDefinition(username, login_Id);

				System.out
						.println("Please, select one of the following measurements to input: ");
				Integer i = 1;
				for (Measuredefinition measureDefinition : mdefs) {
					System.out.println(i++ + ". "
							+ measureDefinition.getMeasureName());
				}

				try {
					i = Integer.parseInt(buffer.readLine());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				Userprofile uP = servicePort.getprofile(username, login_Id);

				System.out.println("Please, provide you measure value: ");
				double val = 0.0;
				try {
					val = Double.parseDouble(buffer.readLine());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				servicePort.savemeasure(username, login_Id, val,
						mdefs.get(i - 1).getMeasureName());

			}
				break;

			case 3: {

				Goalsetting goalSetting = new Goalsetting();
				GregorianCalendar gcal = new GregorianCalendar();

				Userprofile uP = servicePort.getprofile(username, login_Id);

				if (uP.getCGorUP() == 0) {
					// uP is non CareGiver account
					goalSetting.setByCareGiver(0);
					goalSetting.setByUserProfile(1);
				} else {
					goalSetting.setByCareGiver(1);
					goalSetting.setByUserProfile(0);
				}
				// goalSetting.setPRid(value);
				String goalString = "";
				System.out.println("Please, print you goal string: ");
				try {
					goalString = buffer.readLine();
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				goalSetting.setQuery(goalString);

				Integer year = 0, month = 0, date = 0, hourOfDay = 0, minute = 0;

				System.out.println("Please, year for the goal: ");
				try {
					year = Integer.parseInt(buffer.readLine());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				System.out.println("Please, month for the goal: ");
				try {
					month = Integer.parseInt(buffer.readLine());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				System.out.println("Please, date for the goal: ");
				try {
					date = Integer.parseInt(buffer.readLine());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				System.out.println("Please, hourOfDay for the goal: ");
				try {
					hourOfDay = Integer.parseInt(buffer.readLine());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				System.out.println("Please, minute for the goal: ");
				try {
					minute = Integer.parseInt(buffer.readLine());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				System.out
						.println("What service do you want to avail? (e.g. Doctor, medical checkup etc ");
				try {
					goalSetting.setService(buffer.readLine());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				gcal.set(year, month, date, hourOfDay, minute);
				XMLGregorianCalendar xdatetime = DatatypeFactory.newInstance()
						.newXMLGregorianCalendar(gcal);
				goalSetting.setScheduleDateTime(xdatetime);
				goalSetting.setUserprofile(uP);
				goalSetting.setWorkProgress("0");

				Holder<Goalsetting> holder = new Holder<Goalsetting>(
						goalSetting);
				servicePort.setgoal(holder, username, login_Id);

			}
				break;

			case 4: {
				String Message = servicePort.getquote();
				System.out.println(Message);

				System.out
						.println("********************************\nPress Enter after reading : \n");
				try {
					Message = buffer.readLine();
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
				break;

			case 5: {
				// persuasive strategy
				String Message = servicePort.feedback(username, login_Id);
				System.out.println(Message);

				System.out
						.println("********************************\nPress Enter after reading : \n");
				try {
					Message = buffer.readLine();
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
				break;

			case 6: {
				// Advice
				String Message = servicePort.getadvice(username, login_Id);
				System.out.println(Message);

				System.out
						.println("********************************\nPress Enter after reading : \n");
				try {
					Message = buffer.readLine();
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
				break;

			case 7: {
				servicePort.share(username, login_Id);
			}
				break;

			case 8: {
				servicePort.logout(username, login_Id);
				exit = 1;
			}
				break;

			case 9: {
				exit = 1;
			}
				break;

			default: {

			}
				break;
			}

			if (exit == 1) {
				pr_thread.kill();
				break;
			}
		}

		System.out.println("Exiting...");

	}

	private static void login() {
		// TODO Auto-generated method stub
		System.out.println("Please login to avail features: ");
		System.out.println("1. Login");
		// System.out.println();
	}

	private static void printMenuPage() {
		// TODO Auto-generated method stub

		System.out.println("Enter Number from 2 to 6 to select a task :");
		// System.out.println("1. Login");
		System.out.println("2. Input Measure Data");
		System.out.println("3. Set a Goal");
		System.out.println("4. Request awareness News (quote)");
		System.out.println("5. Persuasive strategy");
		System.out.println("6. Feedback");
		System.out.println("7. Share on Twitter");
		System.out.println("8. LogOut");
		System.out.println("9. Exit");

	}

	/**
	 * PushRemainder is working as web polling that communicates with server to
	 * get the latest update about the user's recent goals.
	 * 
	 */
	public static class PushReminders implements Runnable {
		private Thread thread;
		private boolean thread_killer = true;
		private String thread_text = "pr_thread in action...";
		String Message = "";
		Goalsetting goal = null;

		@SuppressWarnings("static-access")
		@Override
		public void run() {
			// TODO Auto-generated method stub

			while (thread_killer) {

				try {

					if (login_Id != "")
						goal = servicePort.pushreminder(username, login_Id);

					if (goal != null) {
						Message = "Service: " + goal.getService();
						Message += "\n" + "Goal: " + goal.getQuery();
						Message += "\n" + "Time: " + goal.getScheduleDateTime();
					}

					if (Message != "" && Message != null) {
						JOptionPane.showMessageDialog(null, Message,
								"Push Reminders",
								JOptionPane.INFORMATION_MESSAGE);
						Message = null;
					}
					// JOptionPane.showMessageDialog(null, Message);

					thread.sleep(PushReminders_Interval * 1000 * 60);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		public void kill() {
			thread_killer = false;
		}

		public void start() {

			if (thread == null) {
				thread = new Thread(this, "PushReminders");
				thread.start();
			}
		}
	}

}