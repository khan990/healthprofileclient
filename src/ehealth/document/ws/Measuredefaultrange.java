
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for measuredefaultrange complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="measuredefaultrange">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="alarmLevel" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="endValue" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="id" type="{http://ws.document.ehealth/}measuredefaultrangePK" minOccurs="0"/>
 *         &lt;element name="measuredefinition" type="{http://ws.document.ehealth/}measuredefinition" minOccurs="0"/>
 *         &lt;element name="rangeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startValue" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "measuredefaultrange", propOrder = {
    "alarmLevel",
    "endValue",
    "id",
    "measuredefinition",
    "rangeName",
    "startValue"
})
public class Measuredefaultrange {

    protected int alarmLevel;
    protected double endValue;
    protected MeasuredefaultrangePK id;
    protected Measuredefinition measuredefinition;
    protected String rangeName;
    protected double startValue;

    /**
     * Gets the value of the alarmLevel property.
     * 
     */
    public int getAlarmLevel() {
        return alarmLevel;
    }

    /**
     * Sets the value of the alarmLevel property.
     * 
     */
    public void setAlarmLevel(int value) {
        this.alarmLevel = value;
    }

    /**
     * Gets the value of the endValue property.
     * 
     */
    public double getEndValue() {
        return endValue;
    }

    /**
     * Sets the value of the endValue property.
     * 
     */
    public void setEndValue(double value) {
        this.endValue = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link MeasuredefaultrangePK }
     *     
     */
    public MeasuredefaultrangePK getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasuredefaultrangePK }
     *     
     */
    public void setId(MeasuredefaultrangePK value) {
        this.id = value;
    }

    /**
     * Gets the value of the measuredefinition property.
     * 
     * @return
     *     possible object is
     *     {@link Measuredefinition }
     *     
     */
    public Measuredefinition getMeasuredefinition() {
        return measuredefinition;
    }

    /**
     * Sets the value of the measuredefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link Measuredefinition }
     *     
     */
    public void setMeasuredefinition(Measuredefinition value) {
        this.measuredefinition = value;
    }

    /**
     * Gets the value of the rangeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRangeName() {
        return rangeName;
    }

    /**
     * Sets the value of the rangeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRangeName(String value) {
        this.rangeName = value;
    }

    /**
     * Gets the value of the startValue property.
     * 
     */
    public double getStartValue() {
        return startValue;
    }

    /**
     * Sets the value of the startValue property.
     * 
     */
    public void setStartValue(double value) {
        this.startValue = value;
    }

}
