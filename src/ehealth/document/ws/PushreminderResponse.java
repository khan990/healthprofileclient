
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pushreminderResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pushreminderResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="push" type="{http://ws.document.ehealth/}goalsetting" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pushreminderResponse", propOrder = {
    "push"
})
public class PushreminderResponse {

    protected Goalsetting push;

    /**
     * Gets the value of the push property.
     * 
     * @return
     *     possible object is
     *     {@link Goalsetting }
     *     
     */
    public Goalsetting getPush() {
        return push;
    }

    /**
     * Sets the value of the push property.
     * 
     * @param value
     *     allowed object is
     *     {@link Goalsetting }
     *     
     */
    public void setPush(Goalsetting value) {
        this.push = value;
    }

}
