
package ehealth.document.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getusergoalsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getusergoalsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usergoals" type="{http://ws.document.ehealth/}goalsetting" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getusergoalsResponse", propOrder = {
    "usergoals"
})
public class GetusergoalsResponse {

    protected List<Goalsetting> usergoals;

    /**
     * Gets the value of the usergoals property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usergoals property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsergoals().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Goalsetting }
     * 
     * 
     */
    public List<Goalsetting> getUsergoals() {
        if (usergoals == null) {
            usergoals = new ArrayList<Goalsetting>();
        }
        return this.usergoals;
    }

}
