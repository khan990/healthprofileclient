
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateGoalResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateGoalResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="goalsettings" type="{http://ws.document.ehealth/}goalsetting" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateGoalResponse", propOrder = {
    "goalsettings"
})
public class UpdateGoalResponse {

    protected Goalsetting goalsettings;

    /**
     * Gets the value of the goalsettings property.
     * 
     * @return
     *     possible object is
     *     {@link Goalsetting }
     *     
     */
    public Goalsetting getGoalsettings() {
        return goalsettings;
    }

    /**
     * Sets the value of the goalsettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link Goalsetting }
     *     
     */
    public void setGoalsettings(Goalsetting value) {
        this.goalsettings = value;
    }

}
