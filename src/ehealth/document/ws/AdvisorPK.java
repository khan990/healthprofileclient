
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for advisorPK complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="advisorPK">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UPid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "advisorPK", propOrder = {
    "aid",
    "uPid"
})
public class AdvisorPK {

    protected int aid;
    @XmlElement(name = "UPid")
    protected int uPid;

    /**
     * Gets the value of the aid property.
     * 
     */
    public int getAid() {
        return aid;
    }

    /**
     * Sets the value of the aid property.
     * 
     */
    public void setAid(int value) {
        this.aid = value;
    }

    /**
     * Gets the value of the uPid property.
     * 
     */
    public int getUPid() {
        return uPid;
    }

    /**
     * Sets the value of the uPid property.
     * 
     */
    public void setUPid(int value) {
        this.uPid = value;
    }

}
