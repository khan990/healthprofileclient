
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for caregiver complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="caregiver">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CGid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="userprofile1" type="{http://ws.document.ehealth/}userprofile" minOccurs="0"/>
 *         &lt;element name="userprofile2" type="{http://ws.document.ehealth/}userprofile" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "caregiver", propOrder = {
    "cGid",
    "userprofile1",
    "userprofile2"
})
public class Caregiver {

    @XmlElement(name = "CGid")
    protected int cGid;
    protected Userprofile userprofile1;
    protected Userprofile userprofile2;

    /**
     * Gets the value of the cGid property.
     * 
     */
    public int getCGid() {
        return cGid;
    }

    /**
     * Sets the value of the cGid property.
     * 
     */
    public void setCGid(int value) {
        this.cGid = value;
    }

    /**
     * Gets the value of the userprofile1 property.
     * 
     * @return
     *     possible object is
     *     {@link Userprofile }
     *     
     */
    public Userprofile getUserprofile1() {
        return userprofile1;
    }

    /**
     * Sets the value of the userprofile1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Userprofile }
     *     
     */
    public void setUserprofile1(Userprofile value) {
        this.userprofile1 = value;
    }

    /**
     * Gets the value of the userprofile2 property.
     * 
     * @return
     *     possible object is
     *     {@link Userprofile }
     *     
     */
    public Userprofile getUserprofile2() {
        return userprofile2;
    }

    /**
     * Sets the value of the userprofile2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Userprofile }
     *     
     */
    public void setUserprofile2(Userprofile value) {
        this.userprofile2 = value;
    }

}
