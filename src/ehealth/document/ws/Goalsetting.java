
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for goalsetting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="goalsetting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="byCareGiver" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="byUserProfile" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PRid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="query" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scheduleDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="service" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userprofile" type="{http://ws.document.ehealth/}userprofile" minOccurs="0"/>
 *         &lt;element name="workProgress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "goalsetting", propOrder = {
    "byCareGiver",
    "byUserProfile",
    "pRid",
    "query",
    "scheduleDateTime",
    "service",
    "userprofile",
    "workProgress"
})
public class Goalsetting {

    protected int byCareGiver;
    protected int byUserProfile;
    @XmlElement(name = "PRid")
    protected int pRid;
    protected String query;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar scheduleDateTime;
    protected String service;
    protected Userprofile userprofile;
    protected String workProgress;

    /**
     * Gets the value of the byCareGiver property.
     * 
     */
    public int getByCareGiver() {
        return byCareGiver;
    }

    /**
     * Sets the value of the byCareGiver property.
     * 
     */
    public void setByCareGiver(int value) {
        this.byCareGiver = value;
    }

    /**
     * Gets the value of the byUserProfile property.
     * 
     */
    public int getByUserProfile() {
        return byUserProfile;
    }

    /**
     * Sets the value of the byUserProfile property.
     * 
     */
    public void setByUserProfile(int value) {
        this.byUserProfile = value;
    }

    /**
     * Gets the value of the pRid property.
     * 
     */
    public int getPRid() {
        return pRid;
    }

    /**
     * Sets the value of the pRid property.
     * 
     */
    public void setPRid(int value) {
        this.pRid = value;
    }

    /**
     * Gets the value of the query property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuery() {
        return query;
    }

    /**
     * Sets the value of the query property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuery(String value) {
        this.query = value;
    }

    /**
     * Gets the value of the scheduleDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getScheduleDateTime() {
        return scheduleDateTime;
    }

    /**
     * Sets the value of the scheduleDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setScheduleDateTime(XMLGregorianCalendar value) {
        this.scheduleDateTime = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getService() {
        return service;
    }

    /**
     * Sets the value of the service property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setService(String value) {
        this.service = value;
    }

    /**
     * Gets the value of the userprofile property.
     * 
     * @return
     *     possible object is
     *     {@link Userprofile }
     *     
     */
    public Userprofile getUserprofile() {
        return userprofile;
    }

    /**
     * Sets the value of the userprofile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Userprofile }
     *     
     */
    public void setUserprofile(Userprofile value) {
        this.userprofile = value;
    }

    /**
     * Gets the value of the workProgress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkProgress() {
        return workProgress;
    }

    /**
     * Sets the value of the workProgress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkProgress(String value) {
        this.workProgress = value;
    }

}
