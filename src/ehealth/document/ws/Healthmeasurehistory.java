
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for healthmeasurehistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="healthmeasurehistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="HMHid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="measuredefinition" type="{http://ws.document.ehealth/}measuredefinition" minOccurs="0"/>
 *         &lt;element name="userprofile" type="{http://ws.document.ehealth/}userprofile" minOccurs="0"/>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "healthmeasurehistory", propOrder = {
    "dateTime",
    "hmHid",
    "measuredefinition",
    "userprofile",
    "value"
})
public class Healthmeasurehistory {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTime;
    @XmlElement(name = "HMHid")
    protected int hmHid;
    protected Measuredefinition measuredefinition;
    protected Userprofile userprofile;
    protected double value;

    /**
     * Gets the value of the dateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTime() {
        return dateTime;
    }

    /**
     * Sets the value of the dateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTime(XMLGregorianCalendar value) {
        this.dateTime = value;
    }

    /**
     * Gets the value of the hmHid property.
     * 
     */
    public int getHMHid() {
        return hmHid;
    }

    /**
     * Sets the value of the hmHid property.
     * 
     */
    public void setHMHid(int value) {
        this.hmHid = value;
    }

    /**
     * Gets the value of the measuredefinition property.
     * 
     * @return
     *     possible object is
     *     {@link Measuredefinition }
     *     
     */
    public Measuredefinition getMeasuredefinition() {
        return measuredefinition;
    }

    /**
     * Sets the value of the measuredefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link Measuredefinition }
     *     
     */
    public void setMeasuredefinition(Measuredefinition value) {
        this.measuredefinition = value;
    }

    /**
     * Gets the value of the userprofile property.
     * 
     * @return
     *     possible object is
     *     {@link Userprofile }
     *     
     */
    public Userprofile getUserprofile() {
        return userprofile;
    }

    /**
     * Sets the value of the userprofile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Userprofile }
     *     
     */
    public void setUserprofile(Userprofile value) {
        this.userprofile = value;
    }

    /**
     * Gets the value of the value property.
     * 
     */
    public double getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     */
    public void setValue(double value) {
        this.value = value;
    }

}
