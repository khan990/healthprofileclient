
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for advisor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="advisor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="advice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="measuredefinition" type="{http://ws.document.ehealth/}measuredefinition" minOccurs="0"/>
 *         &lt;element name="tags" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userprofile" type="{http://ws.document.ehealth/}userprofile" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "advisor", propOrder = {
    "advice",
    "aid",
    "measuredefinition",
    "tags",
    "userprofile"
})
public class Advisor {

    protected String advice;
    protected int aid;
    protected Measuredefinition measuredefinition;
    protected String tags;
    protected Userprofile userprofile;

    /**
     * Gets the value of the advice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvice() {
        return advice;
    }

    /**
     * Sets the value of the advice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvice(String value) {
        this.advice = value;
    }

    /**
     * Gets the value of the aid property.
     * 
     */
    public int getAid() {
        return aid;
    }

    /**
     * Sets the value of the aid property.
     * 
     */
    public void setAid(int value) {
        this.aid = value;
    }

    /**
     * Gets the value of the measuredefinition property.
     * 
     * @return
     *     possible object is
     *     {@link Measuredefinition }
     *     
     */
    public Measuredefinition getMeasuredefinition() {
        return measuredefinition;
    }

    /**
     * Sets the value of the measuredefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link Measuredefinition }
     *     
     */
    public void setMeasuredefinition(Measuredefinition value) {
        this.measuredefinition = value;
    }

    /**
     * Gets the value of the tags property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTags() {
        return tags;
    }

    /**
     * Sets the value of the tags property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTags(String value) {
        this.tags = value;
    }

    /**
     * Gets the value of the userprofile property.
     * 
     * @return
     *     possible object is
     *     {@link Userprofile }
     *     
     */
    public Userprofile getUserprofile() {
        return userprofile;
    }

    /**
     * Sets the value of the userprofile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Userprofile }
     *     
     */
    public void setUserprofile(Userprofile value) {
        this.userprofile = value;
    }

}
