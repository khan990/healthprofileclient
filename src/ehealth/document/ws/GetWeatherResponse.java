
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getweatherResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getweatherResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="weatherreport" type="{http://ws.document.ehealth/}currentWeather" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getweatherResponse", propOrder = {
    "weatherreport"
})
public class GetweatherResponse {

    protected CurrentWeather weatherreport;

    /**
     * Gets the value of the weatherreport property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentWeather }
     *     
     */
    public CurrentWeather getWeatherreport() {
        return weatherreport;
    }

    /**
     * Sets the value of the weatherreport property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentWeather }
     *     
     */
    public void setWeatherreport(CurrentWeather value) {
        this.weatherreport = value;
    }

}
