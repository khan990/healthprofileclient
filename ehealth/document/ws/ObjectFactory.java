
package ehealth.document.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ehealth.document.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Getquote_QNAME = new QName("http://ws.document.ehealth/", "getquote");
    private final static QName _Userprofile_QNAME = new QName("http://ws.document.ehealth/", "userprofile");
    private final static QName _Setgoal_QNAME = new QName("http://ws.document.ehealth/", "setgoal");
    private final static QName _GetquoteResponse_QNAME = new QName("http://ws.document.ehealth/", "getquoteResponse");
    private final static QName _Getweather_QNAME = new QName("http://ws.document.ehealth/", "getweather");
    private final static QName _GetcurrentgoalResponse_QNAME = new QName("http://ws.document.ehealth/", "getcurrentgoalResponse");
    private final static QName _UpdateprofileResponse_QNAME = new QName("http://ws.document.ehealth/", "updateprofileResponse");
    private final static QName _SavemeasureResponse_QNAME = new QName("http://ws.document.ehealth/", "savemeasureResponse");
    private final static QName _Getallgoal_QNAME = new QName("http://ws.document.ehealth/", "getallgoal");
    private final static QName _Logout_QNAME = new QName("http://ws.document.ehealth/", "logout");
    private final static QName _Savemeasure_QNAME = new QName("http://ws.document.ehealth/", "savemeasure");
    private final static QName _Getcurrentgoal_QNAME = new QName("http://ws.document.ehealth/", "getcurrentgoal");
    private final static QName _GetprofileResponse_QNAME = new QName("http://ws.document.ehealth/", "getprofileResponse");
    private final static QName _FeedbackResponse_QNAME = new QName("http://ws.document.ehealth/", "feedbackResponse");
    private final static QName _DeleteUserGoalResponse_QNAME = new QName("http://ws.document.ehealth/", "deleteUserGoalResponse");
    private final static QName _LoginResponse_QNAME = new QName("http://ws.document.ehealth/", "loginResponse");
    private final static QName _CheckLoginStatusResponse_QNAME = new QName("http://ws.document.ehealth/", "checkLoginStatusResponse");
    private final static QName _Deletecurrentlifestatus_QNAME = new QName("http://ws.document.ehealth/", "deletecurrentlifestatus");
    private final static QName _PushreminderResponse_QNAME = new QName("http://ws.document.ehealth/", "pushreminderResponse");
    private final static QName _CreateuserResponse_QNAME = new QName("http://ws.document.ehealth/", "createuserResponse");
    private final static QName _DeleteUserGoal_QNAME = new QName("http://ws.document.ehealth/", "deleteUserGoal");
    private final static QName _Getusers_QNAME = new QName("http://ws.document.ehealth/", "getusers");
    private final static QName _ShareResponse_QNAME = new QName("http://ws.document.ehealth/", "shareResponse");
    private final static QName _Deleteprofile_QNAME = new QName("http://ws.document.ehealth/", "deleteprofile");
    private final static QName _LogoutResponse_QNAME = new QName("http://ws.document.ehealth/", "logoutResponse");
    private final static QName _GetusersResponse_QNAME = new QName("http://ws.document.ehealth/", "getusersResponse");
    private final static QName _Login_QNAME = new QName("http://ws.document.ehealth/", "login");
    private final static QName _FinishGoal_QNAME = new QName("http://ws.document.ehealth/", "finishGoal");
    private final static QName _UpdateGoal_QNAME = new QName("http://ws.document.ehealth/", "updateGoal");
    private final static QName _FinishGoalResponse_QNAME = new QName("http://ws.document.ehealth/", "finishGoalResponse");
    private final static QName _Getprofile_QNAME = new QName("http://ws.document.ehealth/", "getprofile");
    private final static QName _Pushreminder_QNAME = new QName("http://ws.document.ehealth/", "pushreminder");
    private final static QName _UpdateGoalResponse_QNAME = new QName("http://ws.document.ehealth/", "updateGoalResponse");
    private final static QName _Share_QNAME = new QName("http://ws.document.ehealth/", "share");
    private final static QName _Createuser_QNAME = new QName("http://ws.document.ehealth/", "createuser");
    private final static QName _Feedback_QNAME = new QName("http://ws.document.ehealth/", "feedback");
    private final static QName _GetweatherResponse_QNAME = new QName("http://ws.document.ehealth/", "getweatherResponse");
    private final static QName _GetallgoalResponse_QNAME = new QName("http://ws.document.ehealth/", "getallgoalResponse");
    private final static QName _GetAllMeasureDefinitionResponse_QNAME = new QName("http://ws.document.ehealth/", "getAllMeasureDefinitionResponse");
    private final static QName _DeleteprofileResponse_QNAME = new QName("http://ws.document.ehealth/", "deleteprofileResponse");
    private final static QName _Getusergoals_QNAME = new QName("http://ws.document.ehealth/", "getusergoals");
    private final static QName _CheckLoginStatus_QNAME = new QName("http://ws.document.ehealth/", "checkLoginStatus");
    private final static QName _DeletecurrentlifestatusResponse_QNAME = new QName("http://ws.document.ehealth/", "deletecurrentlifestatusResponse");
    private final static QName _Updateprofile_QNAME = new QName("http://ws.document.ehealth/", "updateprofile");
    private final static QName _GetAllMeasureDefinition_QNAME = new QName("http://ws.document.ehealth/", "getAllMeasureDefinition");
    private final static QName _SetgoalResponse_QNAME = new QName("http://ws.document.ehealth/", "setgoalResponse");
    private final static QName _GetusergoalsResponse_QNAME = new QName("http://ws.document.ehealth/", "getusergoalsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ehealth.document.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetusergoalsResponse }
     * 
     */
    public GetusergoalsResponse createGetusergoalsResponse() {
        return new GetusergoalsResponse();
    }

    /**
     * Create an instance of {@link Updateprofile }
     * 
     */
    public Updateprofile createUpdateprofile() {
        return new Updateprofile();
    }

    /**
     * Create an instance of {@link SetgoalResponse }
     * 
     */
    public SetgoalResponse createSetgoalResponse() {
        return new SetgoalResponse();
    }

    /**
     * Create an instance of {@link GetAllMeasureDefinition }
     * 
     */
    public GetAllMeasureDefinition createGetAllMeasureDefinition() {
        return new GetAllMeasureDefinition();
    }

    /**
     * Create an instance of {@link DeletecurrentlifestatusResponse }
     * 
     */
    public DeletecurrentlifestatusResponse createDeletecurrentlifestatusResponse() {
        return new DeletecurrentlifestatusResponse();
    }

    /**
     * Create an instance of {@link CheckLoginStatus }
     * 
     */
    public CheckLoginStatus createCheckLoginStatus() {
        return new CheckLoginStatus();
    }

    /**
     * Create an instance of {@link Getusergoals }
     * 
     */
    public Getusergoals createGetusergoals() {
        return new Getusergoals();
    }

    /**
     * Create an instance of {@link DeleteprofileResponse }
     * 
     */
    public DeleteprofileResponse createDeleteprofileResponse() {
        return new DeleteprofileResponse();
    }

    /**
     * Create an instance of {@link GetAllMeasureDefinitionResponse }
     * 
     */
    public GetAllMeasureDefinitionResponse createGetAllMeasureDefinitionResponse() {
        return new GetAllMeasureDefinitionResponse();
    }

    /**
     * Create an instance of {@link GetallgoalResponse }
     * 
     */
    public GetallgoalResponse createGetallgoalResponse() {
        return new GetallgoalResponse();
    }

    /**
     * Create an instance of {@link GetweatherResponse }
     * 
     */
    public GetweatherResponse createGetweatherResponse() {
        return new GetweatherResponse();
    }

    /**
     * Create an instance of {@link Feedback }
     * 
     */
    public Feedback createFeedback() {
        return new Feedback();
    }

    /**
     * Create an instance of {@link UpdateGoalResponse }
     * 
     */
    public UpdateGoalResponse createUpdateGoalResponse() {
        return new UpdateGoalResponse();
    }

    /**
     * Create an instance of {@link Share }
     * 
     */
    public Share createShare() {
        return new Share();
    }

    /**
     * Create an instance of {@link Createuser }
     * 
     */
    public Createuser createCreateuser() {
        return new Createuser();
    }

    /**
     * Create an instance of {@link Pushreminder }
     * 
     */
    public Pushreminder createPushreminder() {
        return new Pushreminder();
    }

    /**
     * Create an instance of {@link UpdateGoal }
     * 
     */
    public UpdateGoal createUpdateGoal() {
        return new UpdateGoal();
    }

    /**
     * Create an instance of {@link FinishGoalResponse }
     * 
     */
    public FinishGoalResponse createFinishGoalResponse() {
        return new FinishGoalResponse();
    }

    /**
     * Create an instance of {@link Getprofile }
     * 
     */
    public Getprofile createGetprofile() {
        return new Getprofile();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link FinishGoal }
     * 
     */
    public FinishGoal createFinishGoal() {
        return new FinishGoal();
    }

    /**
     * Create an instance of {@link GetusersResponse }
     * 
     */
    public GetusersResponse createGetusersResponse() {
        return new GetusersResponse();
    }

    /**
     * Create an instance of {@link ShareResponse }
     * 
     */
    public ShareResponse createShareResponse() {
        return new ShareResponse();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link Deleteprofile }
     * 
     */
    public Deleteprofile createDeleteprofile() {
        return new Deleteprofile();
    }

    /**
     * Create an instance of {@link Getusers }
     * 
     */
    public Getusers createGetusers() {
        return new Getusers();
    }

    /**
     * Create an instance of {@link DeleteUserGoal }
     * 
     */
    public DeleteUserGoal createDeleteUserGoal() {
        return new DeleteUserGoal();
    }

    /**
     * Create an instance of {@link CreateuserResponse }
     * 
     */
    public CreateuserResponse createCreateuserResponse() {
        return new CreateuserResponse();
    }

    /**
     * Create an instance of {@link PushreminderResponse }
     * 
     */
    public PushreminderResponse createPushreminderResponse() {
        return new PushreminderResponse();
    }

    /**
     * Create an instance of {@link Deletecurrentlifestatus }
     * 
     */
    public Deletecurrentlifestatus createDeletecurrentlifestatus() {
        return new Deletecurrentlifestatus();
    }

    /**
     * Create an instance of {@link CheckLoginStatusResponse }
     * 
     */
    public CheckLoginStatusResponse createCheckLoginStatusResponse() {
        return new CheckLoginStatusResponse();
    }

    /**
     * Create an instance of {@link DeleteUserGoalResponse }
     * 
     */
    public DeleteUserGoalResponse createDeleteUserGoalResponse() {
        return new DeleteUserGoalResponse();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link FeedbackResponse }
     * 
     */
    public FeedbackResponse createFeedbackResponse() {
        return new FeedbackResponse();
    }

    /**
     * Create an instance of {@link Getcurrentgoal }
     * 
     */
    public Getcurrentgoal createGetcurrentgoal() {
        return new Getcurrentgoal();
    }

    /**
     * Create an instance of {@link GetprofileResponse }
     * 
     */
    public GetprofileResponse createGetprofileResponse() {
        return new GetprofileResponse();
    }

    /**
     * Create an instance of {@link Savemeasure }
     * 
     */
    public Savemeasure createSavemeasure() {
        return new Savemeasure();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link Getallgoal }
     * 
     */
    public Getallgoal createGetallgoal() {
        return new Getallgoal();
    }

    /**
     * Create an instance of {@link UpdateprofileResponse }
     * 
     */
    public UpdateprofileResponse createUpdateprofileResponse() {
        return new UpdateprofileResponse();
    }

    /**
     * Create an instance of {@link GetcurrentgoalResponse }
     * 
     */
    public GetcurrentgoalResponse createGetcurrentgoalResponse() {
        return new GetcurrentgoalResponse();
    }

    /**
     * Create an instance of {@link SavemeasureResponse }
     * 
     */
    public SavemeasureResponse createSavemeasureResponse() {
        return new SavemeasureResponse();
    }

    /**
     * Create an instance of {@link Getweather }
     * 
     */
    public Getweather createGetweather() {
        return new Getweather();
    }

    /**
     * Create an instance of {@link GetquoteResponse }
     * 
     */
    public GetquoteResponse createGetquoteResponse() {
        return new GetquoteResponse();
    }

    /**
     * Create an instance of {@link Setgoal }
     * 
     */
    public Setgoal createSetgoal() {
        return new Setgoal();
    }

    /**
     * Create an instance of {@link Userprofile }
     * 
     */
    public Userprofile createUserprofile() {
        return new Userprofile();
    }

    /**
     * Create an instance of {@link Getquote }
     * 
     */
    public Getquote createGetquote() {
        return new Getquote();
    }

    /**
     * Create an instance of {@link Currentlifestatus }
     * 
     */
    public Currentlifestatus createCurrentlifestatus() {
        return new Currentlifestatus();
    }

    /**
     * Create an instance of {@link MeasuredefaultrangePK }
     * 
     */
    public MeasuredefaultrangePK createMeasuredefaultrangePK() {
        return new MeasuredefaultrangePK();
    }

    /**
     * Create an instance of {@link Measuredefinition }
     * 
     */
    public Measuredefinition createMeasuredefinition() {
        return new Measuredefinition();
    }

    /**
     * Create an instance of {@link Measuredefaultrange }
     * 
     */
    public Measuredefaultrange createMeasuredefaultrange() {
        return new Measuredefaultrange();
    }

    /**
     * Create an instance of {@link Healthmeasurehistory }
     * 
     */
    public Healthmeasurehistory createHealthmeasurehistory() {
        return new Healthmeasurehistory();
    }

    /**
     * Create an instance of {@link CurrentWeather }
     * 
     */
    public CurrentWeather createCurrentWeather() {
        return new CurrentWeather();
    }

    /**
     * Create an instance of {@link Goalsetting }
     * 
     */
    public Goalsetting createGoalsetting() {
        return new Goalsetting();
    }

    /**
     * Create an instance of {@link Advisor }
     * 
     */
    public Advisor createAdvisor() {
        return new Advisor();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getquote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getquote")
    public JAXBElement<Getquote> createGetquote(Getquote value) {
        return new JAXBElement<Getquote>(_Getquote_QNAME, Getquote.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Userprofile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "userprofile")
    public JAXBElement<Userprofile> createUserprofile(Userprofile value) {
        return new JAXBElement<Userprofile>(_Userprofile_QNAME, Userprofile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Setgoal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "setgoal")
    public JAXBElement<Setgoal> createSetgoal(Setgoal value) {
        return new JAXBElement<Setgoal>(_Setgoal_QNAME, Setgoal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetquoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getquoteResponse")
    public JAXBElement<GetquoteResponse> createGetquoteResponse(GetquoteResponse value) {
        return new JAXBElement<GetquoteResponse>(_GetquoteResponse_QNAME, GetquoteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getweather }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getweather")
    public JAXBElement<Getweather> createGetweather(Getweather value) {
        return new JAXBElement<Getweather>(_Getweather_QNAME, Getweather.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetcurrentgoalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getcurrentgoalResponse")
    public JAXBElement<GetcurrentgoalResponse> createGetcurrentgoalResponse(GetcurrentgoalResponse value) {
        return new JAXBElement<GetcurrentgoalResponse>(_GetcurrentgoalResponse_QNAME, GetcurrentgoalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateprofileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "updateprofileResponse")
    public JAXBElement<UpdateprofileResponse> createUpdateprofileResponse(UpdateprofileResponse value) {
        return new JAXBElement<UpdateprofileResponse>(_UpdateprofileResponse_QNAME, UpdateprofileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SavemeasureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "savemeasureResponse")
    public JAXBElement<SavemeasureResponse> createSavemeasureResponse(SavemeasureResponse value) {
        return new JAXBElement<SavemeasureResponse>(_SavemeasureResponse_QNAME, SavemeasureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getallgoal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getallgoal")
    public JAXBElement<Getallgoal> createGetallgoal(Getallgoal value) {
        return new JAXBElement<Getallgoal>(_Getallgoal_QNAME, Getallgoal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Savemeasure }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "savemeasure")
    public JAXBElement<Savemeasure> createSavemeasure(Savemeasure value) {
        return new JAXBElement<Savemeasure>(_Savemeasure_QNAME, Savemeasure.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getcurrentgoal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getcurrentgoal")
    public JAXBElement<Getcurrentgoal> createGetcurrentgoal(Getcurrentgoal value) {
        return new JAXBElement<Getcurrentgoal>(_Getcurrentgoal_QNAME, Getcurrentgoal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetprofileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getprofileResponse")
    public JAXBElement<GetprofileResponse> createGetprofileResponse(GetprofileResponse value) {
        return new JAXBElement<GetprofileResponse>(_GetprofileResponse_QNAME, GetprofileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeedbackResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "feedbackResponse")
    public JAXBElement<FeedbackResponse> createFeedbackResponse(FeedbackResponse value) {
        return new JAXBElement<FeedbackResponse>(_FeedbackResponse_QNAME, FeedbackResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserGoalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "deleteUserGoalResponse")
    public JAXBElement<DeleteUserGoalResponse> createDeleteUserGoalResponse(DeleteUserGoalResponse value) {
        return new JAXBElement<DeleteUserGoalResponse>(_DeleteUserGoalResponse_QNAME, DeleteUserGoalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckLoginStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "checkLoginStatusResponse")
    public JAXBElement<CheckLoginStatusResponse> createCheckLoginStatusResponse(CheckLoginStatusResponse value) {
        return new JAXBElement<CheckLoginStatusResponse>(_CheckLoginStatusResponse_QNAME, CheckLoginStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Deletecurrentlifestatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "deletecurrentlifestatus")
    public JAXBElement<Deletecurrentlifestatus> createDeletecurrentlifestatus(Deletecurrentlifestatus value) {
        return new JAXBElement<Deletecurrentlifestatus>(_Deletecurrentlifestatus_QNAME, Deletecurrentlifestatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PushreminderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "pushreminderResponse")
    public JAXBElement<PushreminderResponse> createPushreminderResponse(PushreminderResponse value) {
        return new JAXBElement<PushreminderResponse>(_PushreminderResponse_QNAME, PushreminderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateuserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "createuserResponse")
    public JAXBElement<CreateuserResponse> createCreateuserResponse(CreateuserResponse value) {
        return new JAXBElement<CreateuserResponse>(_CreateuserResponse_QNAME, CreateuserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserGoal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "deleteUserGoal")
    public JAXBElement<DeleteUserGoal> createDeleteUserGoal(DeleteUserGoal value) {
        return new JAXBElement<DeleteUserGoal>(_DeleteUserGoal_QNAME, DeleteUserGoal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getusers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getusers")
    public JAXBElement<Getusers> createGetusers(Getusers value) {
        return new JAXBElement<Getusers>(_Getusers_QNAME, Getusers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShareResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "shareResponse")
    public JAXBElement<ShareResponse> createShareResponse(ShareResponse value) {
        return new JAXBElement<ShareResponse>(_ShareResponse_QNAME, ShareResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Deleteprofile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "deleteprofile")
    public JAXBElement<Deleteprofile> createDeleteprofile(Deleteprofile value) {
        return new JAXBElement<Deleteprofile>(_Deleteprofile_QNAME, Deleteprofile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetusersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getusersResponse")
    public JAXBElement<GetusersResponse> createGetusersResponse(GetusersResponse value) {
        return new JAXBElement<GetusersResponse>(_GetusersResponse_QNAME, GetusersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishGoal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "finishGoal")
    public JAXBElement<FinishGoal> createFinishGoal(FinishGoal value) {
        return new JAXBElement<FinishGoal>(_FinishGoal_QNAME, FinishGoal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateGoal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "updateGoal")
    public JAXBElement<UpdateGoal> createUpdateGoal(UpdateGoal value) {
        return new JAXBElement<UpdateGoal>(_UpdateGoal_QNAME, UpdateGoal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinishGoalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "finishGoalResponse")
    public JAXBElement<FinishGoalResponse> createFinishGoalResponse(FinishGoalResponse value) {
        return new JAXBElement<FinishGoalResponse>(_FinishGoalResponse_QNAME, FinishGoalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getprofile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getprofile")
    public JAXBElement<Getprofile> createGetprofile(Getprofile value) {
        return new JAXBElement<Getprofile>(_Getprofile_QNAME, Getprofile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Pushreminder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "pushreminder")
    public JAXBElement<Pushreminder> createPushreminder(Pushreminder value) {
        return new JAXBElement<Pushreminder>(_Pushreminder_QNAME, Pushreminder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateGoalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "updateGoalResponse")
    public JAXBElement<UpdateGoalResponse> createUpdateGoalResponse(UpdateGoalResponse value) {
        return new JAXBElement<UpdateGoalResponse>(_UpdateGoalResponse_QNAME, UpdateGoalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Share }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "share")
    public JAXBElement<Share> createShare(Share value) {
        return new JAXBElement<Share>(_Share_QNAME, Share.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Createuser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "createuser")
    public JAXBElement<Createuser> createCreateuser(Createuser value) {
        return new JAXBElement<Createuser>(_Createuser_QNAME, Createuser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Feedback }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "feedback")
    public JAXBElement<Feedback> createFeedback(Feedback value) {
        return new JAXBElement<Feedback>(_Feedback_QNAME, Feedback.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetweatherResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getweatherResponse")
    public JAXBElement<GetweatherResponse> createGetweatherResponse(GetweatherResponse value) {
        return new JAXBElement<GetweatherResponse>(_GetweatherResponse_QNAME, GetweatherResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetallgoalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getallgoalResponse")
    public JAXBElement<GetallgoalResponse> createGetallgoalResponse(GetallgoalResponse value) {
        return new JAXBElement<GetallgoalResponse>(_GetallgoalResponse_QNAME, GetallgoalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllMeasureDefinitionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getAllMeasureDefinitionResponse")
    public JAXBElement<GetAllMeasureDefinitionResponse> createGetAllMeasureDefinitionResponse(GetAllMeasureDefinitionResponse value) {
        return new JAXBElement<GetAllMeasureDefinitionResponse>(_GetAllMeasureDefinitionResponse_QNAME, GetAllMeasureDefinitionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteprofileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "deleteprofileResponse")
    public JAXBElement<DeleteprofileResponse> createDeleteprofileResponse(DeleteprofileResponse value) {
        return new JAXBElement<DeleteprofileResponse>(_DeleteprofileResponse_QNAME, DeleteprofileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getusergoals }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getusergoals")
    public JAXBElement<Getusergoals> createGetusergoals(Getusergoals value) {
        return new JAXBElement<Getusergoals>(_Getusergoals_QNAME, Getusergoals.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckLoginStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "checkLoginStatus")
    public JAXBElement<CheckLoginStatus> createCheckLoginStatus(CheckLoginStatus value) {
        return new JAXBElement<CheckLoginStatus>(_CheckLoginStatus_QNAME, CheckLoginStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletecurrentlifestatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "deletecurrentlifestatusResponse")
    public JAXBElement<DeletecurrentlifestatusResponse> createDeletecurrentlifestatusResponse(DeletecurrentlifestatusResponse value) {
        return new JAXBElement<DeletecurrentlifestatusResponse>(_DeletecurrentlifestatusResponse_QNAME, DeletecurrentlifestatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Updateprofile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "updateprofile")
    public JAXBElement<Updateprofile> createUpdateprofile(Updateprofile value) {
        return new JAXBElement<Updateprofile>(_Updateprofile_QNAME, Updateprofile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllMeasureDefinition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getAllMeasureDefinition")
    public JAXBElement<GetAllMeasureDefinition> createGetAllMeasureDefinition(GetAllMeasureDefinition value) {
        return new JAXBElement<GetAllMeasureDefinition>(_GetAllMeasureDefinition_QNAME, GetAllMeasureDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetgoalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "setgoalResponse")
    public JAXBElement<SetgoalResponse> createSetgoalResponse(SetgoalResponse value) {
        return new JAXBElement<SetgoalResponse>(_SetgoalResponse_QNAME, SetgoalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetusergoalsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.document.ehealth/", name = "getusergoalsResponse")
    public JAXBElement<GetusergoalsResponse> createGetusergoalsResponse(GetusergoalsResponse value) {
        return new JAXBElement<GetusergoalsResponse>(_GetusergoalsResponse_QNAME, GetusergoalsResponse.class, null, value);
    }

}
