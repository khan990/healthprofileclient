
package ehealth.document.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for measuredefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="measuredefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="advisors" type="{http://ws.document.ehealth/}advisor" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="currentlifestatuses" type="{http://ws.document.ehealth/}currentlifestatus" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="healthmeasurehistories" type="{http://ws.document.ehealth/}healthmeasurehistory" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MDid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="measureName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="measureType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="measuredefaultranges" type="{http://ws.document.ehealth/}measuredefaultrange" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "measuredefinition", propOrder = {
    "advisors",
    "currentlifestatuses",
    "healthmeasurehistories",
    "mDid",
    "measureName",
    "measureType",
    "measuredefaultranges"
})
public class Measuredefinition {

    @XmlElement(nillable = true)
    protected List<Advisor> advisors;
    @XmlElement(nillable = true)
    protected List<Currentlifestatus> currentlifestatuses;
    @XmlElement(nillable = true)
    protected List<Healthmeasurehistory> healthmeasurehistories;
    @XmlElement(name = "MDid")
    protected int mDid;
    protected String measureName;
    protected String measureType;
    @XmlElement(nillable = true)
    protected List<Measuredefaultrange> measuredefaultranges;

    /**
     * Gets the value of the advisors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the advisors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdvisors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Advisor }
     * 
     * 
     */
    public List<Advisor> getAdvisors() {
        if (advisors == null) {
            advisors = new ArrayList<Advisor>();
        }
        return this.advisors;
    }

    /**
     * Gets the value of the currentlifestatuses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the currentlifestatuses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCurrentlifestatuses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Currentlifestatus }
     * 
     * 
     */
    public List<Currentlifestatus> getCurrentlifestatuses() {
        if (currentlifestatuses == null) {
            currentlifestatuses = new ArrayList<Currentlifestatus>();
        }
        return this.currentlifestatuses;
    }

    /**
     * Gets the value of the healthmeasurehistories property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the healthmeasurehistories property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHealthmeasurehistories().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Healthmeasurehistory }
     * 
     * 
     */
    public List<Healthmeasurehistory> getHealthmeasurehistories() {
        if (healthmeasurehistories == null) {
            healthmeasurehistories = new ArrayList<Healthmeasurehistory>();
        }
        return this.healthmeasurehistories;
    }

    /**
     * Gets the value of the mDid property.
     * 
     */
    public int getMDid() {
        return mDid;
    }

    /**
     * Sets the value of the mDid property.
     * 
     */
    public void setMDid(int value) {
        this.mDid = value;
    }

    /**
     * Gets the value of the measureName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasureName() {
        return measureName;
    }

    /**
     * Sets the value of the measureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasureName(String value) {
        this.measureName = value;
    }

    /**
     * Gets the value of the measureType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasureType() {
        return measureType;
    }

    /**
     * Sets the value of the measureType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasureType(String value) {
        this.measureType = value;
    }

    /**
     * Gets the value of the measuredefaultranges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the measuredefaultranges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeasuredefaultranges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Measuredefaultrange }
     * 
     * 
     */
    public List<Measuredefaultrange> getMeasuredefaultranges() {
        if (measuredefaultranges == null) {
            measuredefaultranges = new ArrayList<Measuredefaultrange>();
        }
        return this.measuredefaultranges;
    }

}
