
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for savemeasureResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="savemeasureResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lifestatus" type="{http://ws.document.ehealth/}currentlifestatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "savemeasureResponse", propOrder = {
    "lifestatus"
})
public class SavemeasureResponse {

    protected Currentlifestatus lifestatus;

    /**
     * Gets the value of the lifestatus property.
     * 
     * @return
     *     possible object is
     *     {@link Currentlifestatus }
     *     
     */
    public Currentlifestatus getLifestatus() {
        return lifestatus;
    }

    /**
     * Sets the value of the lifestatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Currentlifestatus }
     *     
     */
    public void setLifestatus(Currentlifestatus value) {
        this.lifestatus = value;
    }

}
