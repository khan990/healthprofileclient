
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for measuredefaultrangePK complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="measuredefaultrangePK">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MDRid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MDid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "measuredefaultrangePK", propOrder = {
    "mdRid",
    "mDid"
})
public class MeasuredefaultrangePK {

    @XmlElement(name = "MDRid")
    protected int mdRid;
    @XmlElement(name = "MDid")
    protected int mDid;

    /**
     * Gets the value of the mdRid property.
     * 
     */
    public int getMDRid() {
        return mdRid;
    }

    /**
     * Sets the value of the mdRid property.
     * 
     */
    public void setMDRid(int value) {
        this.mdRid = value;
    }

    /**
     * Gets the value of the mDid property.
     * 
     */
    public int getMDid() {
        return mDid;
    }

    /**
     * Sets the value of the mDid property.
     * 
     */
    public void setMDid(int value) {
        this.mDid = value;
    }

}
