
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for finishGoalResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="finishGoalResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="goalsetting" type="{http://ws.document.ehealth/}goalsetting" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "finishGoalResponse", propOrder = {
    "goalsetting"
})
public class FinishGoalResponse {

    protected Goalsetting goalsetting;

    /**
     * Gets the value of the goalsetting property.
     * 
     * @return
     *     possible object is
     *     {@link Goalsetting }
     *     
     */
    public Goalsetting getGoalsetting() {
        return goalsetting;
    }

    /**
     * Sets the value of the goalsetting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Goalsetting }
     *     
     */
    public void setGoalsetting(Goalsetting value) {
        this.goalsetting = value;
    }

}
