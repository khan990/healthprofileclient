
package ehealth.document.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for checkLoginStatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="checkLoginStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="checklogin" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkLoginStatusResponse", propOrder = {
    "checklogin"
})
public class CheckLoginStatusResponse {

    protected int checklogin;

    /**
     * Gets the value of the checklogin property.
     * 
     */
    public int getChecklogin() {
        return checklogin;
    }

    /**
     * Sets the value of the checklogin property.
     * 
     */
    public void setChecklogin(int value) {
        this.checklogin = value;
    }

}
